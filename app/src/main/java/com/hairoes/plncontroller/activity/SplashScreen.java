package com.hairoes.plncontroller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.RequestHandler;

public class SplashScreen extends Activity {
    private static int SPLASH_TIME_OUT = 2000;
    private SharedPreferences pref;
    final String TAG = this.getClass().getName();
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
                editor = pref.edit();

                final String username = pref.getString("username", "");
                final String password = pref.getString("username", "");
                if (!(username.equals("") && password.equals(""))) {

                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    intent.putExtra("username", username);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
