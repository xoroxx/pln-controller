package com.hairoes.plncontroller.model;

/**
 * Created by hairoes on 28-Jul-16.
 */
public class ListData {
    public String id;
    public String kwh;
    public String air;
    public String bkr;
    public String user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String tgl;
    public String pr;
    public String ps;
    public String pt;

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    public String getKwh() {
        return kwh;
    }

    public void setBkr(String bkr) {
        this.bkr = bkr;
    }

    public String getBkr() {
        return bkr;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getAir() {
        return air;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getPr() {
        return pr;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getPs() {
        return ps;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public String getPt() {
        return pt;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
